# Ola Interpreting Staging

Pre production version for OlaInterpreting.com website served at staging.olainterpreting.com

```
0.6.22 - Minor upgrades
0.5.22 - Russian and Korean language support
0.4.22 - Addressed feedback, support for French language
0.3.22 - Fixed analytics
0.2.22 - CI/CD Configuration
0.1.22 - Initial version
```
