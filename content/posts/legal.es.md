---
title: "Servicios Legales"
date: 2021-09-21T21:25:28-07:00
draft: false
featured_image: '/images/capitol.jpg'
description: Nuestra oferta de servicios legales
---

{{< customimg src="/images/ola.svg" width="50%" >}}
{{< head 2 "Ofrecemos Servicios de Interpretación Legal en los Siguientes Campos" >}}

- Ley Corporativa
- Ley de Empleo
- Ley de Asegurandoras
- Ley de Inmigración
- Servicios Legales de Investigación
- Mediación
- Ley de Protección al Trabajador
- Ley de Bienes Raíces
- Documentos de Acuerdos Legales
- Ley de Compensación al Trabajador

{{< contactbtn email=sales@olainterpreting.com lang=es >}}

{{< pageStats >}}
{{< sharethisbtn >}}

{{< unsplash "es" "og6WD8sDkOM" "onthesearchforpineapples" "Unsplash: Capitolio de Denver" >}}
